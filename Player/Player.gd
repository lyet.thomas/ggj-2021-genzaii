extends KinematicBody2D


onready var animationTree = $AnimationTree
onready var animationState = animationTree.get("parameters/playback")

export var ACCELERATION = 1000
export var MAX_SPEED = 300
export var ROLL_SPEED = 120
export var FRICTION = 2500

var isPaused = false

enum {
	MOVE,
	ROLL,
	ATTACK
}

var state = MOVE
var velocity = Vector2.ZERO

var nakedStr = ""

func _physics_process(delta):
	if isPaused:
		animationState.travel(str(nakedStr, "Idle"))
		move_and_slide(Vector2.ZERO)
		return
	
	var input_vector = Vector2.ZERO
	input_vector.x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	input_vector.y = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
	input_vector = input_vector.normalized()
	
	if input_vector != Vector2.ZERO:
		animationTree.set("parameters/Idle/blend_position", input_vector)
		animationTree.set("parameters/Walk/blend_position", input_vector)
		animationTree.set("parameters/NakedIdle/blend_position", input_vector)
		animationTree.set("parameters/NakedWalk/blend_position", input_vector)
		animationState.travel(str(nakedStr, "Walk"))
		velocity = velocity.move_toward(input_vector * MAX_SPEED, ACCELERATION * delta)
	else:
		animationState.travel(str(nakedStr, "Idle"))
		velocity = velocity.move_toward(Vector2.ZERO, FRICTION * delta)
	
	velocity = move_and_slide(velocity)

func setNaked():
	nakedStr = "Naked"
	animationState.travel("NakedIdle")
