extends "res://Objet/Recepteur.gd"


onready var tvOpen = $TVOpen
onready var tvShut = $TVShut

func reception() -> void :
	if canPlayerDoTheAction:
		$TVOpen.visible = !$TVOpen.visible
		$TVShut.visible = !$TVShut.visible
		if $TVShut.visible:
			emit_signal("quest", 1)

func _on_Timer_timeout() -> void:
	pass # Replace with function body.
