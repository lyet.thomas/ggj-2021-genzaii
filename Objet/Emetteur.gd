extends Node2D

signal emission
signal player_entered
signal player_exited

func startsignal() -> void:
	emit_signal("emission")

func _on_Emetteur_body_entered(_body: Node) -> void:
	emit_signal("player_entered", self)

func _on_Emetteur_body_exited(_body: Node) -> void:
	emit_signal("player_exited", self)

func hasBeenReached() -> void:
	pass


