extends Node2D

signal quest(nb)
signal removeFromInventory()

func reception() -> void:
	pass

func _on_Emetteur_emission() -> void:
	reception()

var canPlayerDoTheAction = false

func _on_Recepteur_body_entered(_body: Node) -> void:
	canPlayerDoTheAction = true

func _on_Recepteur_body_exited(_body: Node) -> void:
	canPlayerDoTheAction = false

