extends "res://Objet/Recepteur.gd"

func reception() -> void:
	emit_signal("quest", 2)
	get_parent().get_node("Emetteur/Sprite").visible = false
	emit_signal("removeFromInventory")
