extends Node2D

var inventoryPosition = Vector2.ZERO

func _process(_delta: float) -> void:
	inventoryPosition = $Player.global_position -  (get_viewport_rect().size / 3)
	if currentInventory != null:
		currentInventory.global_position = inventoryPosition + Vector2(22.5,22.5)

func action() -> void:
	if currentInventory != null:
		currentInventory.startsignal()

###### Management des quetes
var previousQuestNb : int
onready var currentQuestNb : int = 0
onready var currentQuestNode
onready var questLabel = $Player/Polygon2D/RichTextLabel
onready var questRng = RandomNumberGenerator.new()
export(int) var numberOfQuest 
var questReceptacle : Receptacle

onready var roundTimer : int = 120
var score = 0

func _input(event: InputEvent) -> void:
	if event is InputEventKey && event.scancode == KEY_SPACE && $HUD/FinalScoreLabel.visible:
		get_tree().reload_current_scene()

func _ready() -> void:
	$AudioStreamPlayer.playing = true
	
	randomize()
	questRng.randomize()
	questOrder = range(0, numberOfQuest)
	
	currentInventory = get_node("Objets/0/Emetteur/Sprite")
	
	for i in range(1, numberOfQuest):
		var receptacle = get_node(str("Receptacles/", i))
		receptacle.questNumberInt = i
	nextQuest()

func _on_RoundTimer_timeout() -> void:
	if (roundTimer == 0):
		return
	roundTimer -= 1
	if roundTimer == 0:
		EndOfRound()
	$HUD/TimeLabel.text = str("Timer: ", roundTimer)
	pass # Replace with function body.

func EndOfRound() :
	pause()
	$HUD/ScoreLabel.visible = false
	$HUD/TimeLabel.visible = true
	$HUD/Polygon2D.visible = true
	$HUD/FinalScoreLabel.text = str("Score: ", score, "\nAppuyer sur espace pour relancer une partie")
	$HUD/FinalScoreLabel.visible = true
	pass

func nextQuest() -> void:
	# Generation de la prochaine quete
	previousQuestNb = currentQuestNb
	currentQuestNb = nextQuestIndex()
	if previousQuestNb == currentQuestNb:
		currentQuestNb = (currentQuestNb + 1) % numberOfQuest
		
	# Recherche de la prochaine quête
	currentQuestNode = get_node(str("Objets/", currentQuestNb))
	var receptacles : Array = get_tree().get_nodes_in_group("receptacle")
	for receptacle in receptacles:
		if (receptacle as Receptacle).questNumberInt == currentQuestNb:
			questReceptacle = receptacle
			break
	
	# Activation de la zone de détection
	questReceptacle.connect("body_entered", self, "receptacleDetection")
	
	# MaJ du texte
	questLabel.text = (currentQuestNode as MyObject).questText


var currentInventory : Sprite
func putInInventary(newSprite : Sprite) -> void:
	currentInventory.global_position = questReceptacle.get_node("CollisionShape2D").global_position
	currentInventory = newSprite

func receptacleDetection(_body : Node) :
	$Success1.play()
	#Swap d'inventaire
	putInInventary(currentQuestNode.get_node("Emetteur/Sprite"))
	# MaJ de l'ID du receptacle
	questReceptacle.questNumberInt = previousQuestNb
	# Desactivation de la zone de détection du receptacle
	questReceptacle.disconnect("body_entered", self, "receptacleDetection")
	# Activation de la zone de détection du recepteur
	currentQuestNode.get_node("Recepteur").connect("body_entered", self, "recepteurDetection")
	pass

func recepteurDetection(_body : Node) :
	$Success2.play()
	score += 1
	$HUD/ScoreLabel.text = str("Score: ", score)
	# Desactivation de la zone de détection du receptacle
	currentQuestNode.get_node("Recepteur").disconnect("body_entered", self, "recepteurDetection")
	# Pause / Animation / Pause
	animationPostRecepteur()

func pause() -> void:
	$Player.isPaused = !$Player.isPaused 
	if $Player.isPaused:
		$Player.velocity = Vector2.ZERO

#### random quest

var questOrder : Array
onready var questOrderIndex = -1

###################
###################
###################
###################
###################
func nextQuestIndex() -> int:
	#return 8
	if (questOrderIndex == -1 || questOrderIndex == numberOfQuest):
		questOrder.shuffle()
		if questOrderIndex == -1 :
			var index = questOrder.find(8)
			questOrder[index] = questOrder[8]
			questOrder[8] = 8
		questOrderIndex = 0
	var result = questOrder[questOrderIndex]
	questOrderIndex += 1
	return result

func animationPostRecepteur():
	pause()
	match currentQuestNb:
		0: # sac
			$Player/Sprite.visible = false
			$Invisible/StudySheetSprite.visible = true
			$Invisible/StudySprite.visible = true
			$Invisible/StudyAudio.play()
			pass
		1: # television
			$Invisible/TVONSprite.visible = true
			$Invisible/TVAudio.play()
			pass
		2: # verre
			$Invisible/TapWater.visible = true
			$Invisible/TapWaterAudio.play()
			pass
		3: # haltere
			$Player/Sprite.visible = false
			$Invisible/SportSprite.visible = true
		4: # PQ
			$Player/Sprite.visible = false
			$Invisible/AuChiotteSprite.visible = true
			pass
		5: # Panier Linge
			$Invisible/PanierVideSprite.visible = true
			$Invisible/MachineALaverAudio.play()
			pass
		6: # Livre
			$Player/Sprite.visible = false
			$Invisible/ReadingSprite.visible = true
			$Invisible/ReadingAudio.play()
			pass
		7: # Telephone
			$Player/Sprite.visible = false
			$Invisible/PhoneSprite.visible = true
			$Invisible/PhoneAudio.play()
			pass
		8: #Douche
			$Player/Sprite.visible = false
			$Invisible/DoucheSprite.visible = true
			$Invisible/DoucheAudio.play()
			pass
	$AnimationTimer.start()

func _on_AnimationTimer_timeout() -> void:
	match currentQuestNb:
		0: # sac
			$Player/Sprite.visible = true
			$Invisible/StudySheetSprite.visible = false
			$Invisible/StudySprite.visible = false
		1: # television
			$Invisible/TVONSprite.visible = false
			pass
		2: # verre
			$Invisible/TapWater.visible = false
			pass
		3: # haltere
			$Player/Sprite.visible = true
			$Invisible/SportSprite.visible = false
			pass
		4: # PQ
			$Player/Sprite.visible = true
			$Invisible/AuChiotteSprite.visible = false
			$Invisible/AuChiotteAudio.play()
			pass
		5: # Panier Linge
			$Invisible/PanierVideSprite.visible = false
			pass
		6: # Livre
			$Player/Sprite.visible = true
			$Invisible/ReadingSprite.visible = false
			pass
		7: # Telephone
			$Player/Sprite.visible = true
			$Invisible/PhoneSprite.visible = false
			pass
		8: # Douche
			$Player.setNaked()
			$Player/Sprite.visible = true
			$Invisible/DoucheSprite.visible = false
	pause()
	nextQuest()
