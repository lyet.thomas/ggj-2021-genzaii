extends Node2D

onready var startButton : Button = $CanvasLayer/StartButton
onready var creditButton : Button = $CanvasLayer/CreditButton
onready var creditLabel : Label = $CanvasLayer/CreditLabel
onready var polygon : Polygon2D = $CanvasLayer/Polygon2D


func _on_CreditButton_pressed() -> void:
	$CanvasLayer/CreditButton/AudioStreamPlayer.play()
	startButton.visible = false
	creditButton.visible = false
	creditLabel.visible = true
	polygon.visible = true

func _input(event):
	if (event is InputEventMouseButton) && creditLabel.visible:
		startButton.visible = true
		creditButton.visible = true
		creditLabel.visible = false       
		polygon.visible = false


func _on_StartButton_pressed() -> void:
	$CanvasLayer/StartButton/AudioStreamPlayer.play()
	var t = Timer.new()
	t.set_wait_time(1.5)
	t.set_one_shot(true)
	self.add_child(t)
	t.start()
	yield(t, "timeout")
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://World/World.tscn")
